<?php
class Aurel_Cano301products_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_View
{
	protected function _prepareLayout()
    {
        $this->getLayout()->createBlock('catalog/breadcrumbs');
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $product = $this->getProduct();
            $title = $product->getMetaTitle();
            if ($title) {
                $headBlock->setTitle($title);
            }
            $keyword = $product->getMetaKeyword();
            $currentCategory = Mage::registry('current_category');
            if ($keyword) {
                $headBlock->setKeywords($keyword);
            } elseif($currentCategory) {
                $headBlock->setKeywords($product->getName());
            }
            $description = $product->getMetaDescription();
            if ($description) {
                $headBlock->setDescription( ($description) );
            } else {
                $headBlock->setDescription(Mage::helper('core/string')->substr($product->getDescription(), 0, 255));
            }
			$params = array('_ignore_category'=>true);
			if($product->getUrlModel()->getUrl($product, $params) != Mage::helper('core/url')->getCurrentUrl()) {
				//Mage::app()->getResponse()->setRedirect($product->getUrlModel()->getUrl($product, $params), 301);
				header ('HTTP/1.1 301 Moved Permanently');
				header ('Location: ' . $product->getUrlModel()->getUrl($product, $params));
				exit;
			}
        }

        return $this;
    }
}