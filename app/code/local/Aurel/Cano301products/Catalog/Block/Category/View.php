<?php
class Aurel_Cano301products_Catalog_Block_Category_View extends Mage_Catalog_Block_Category_View
{
	protected function _prepareLayout()
    {
		/*if($category->getUrl() != Mage::helper('core/url')->getCurrentUrl()) {
			//Mage::app()->getResponse()->setRedirect($product->getUrlModel()->getUrl($product, $params), 301);
			header ('HTTP/1.1 301 Moved Permanently');
			header ('Location: ' . $category->getUrl());
			exit;
		}*/

        $this->getLayout()->createBlock('catalog/breadcrumbs');

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $category = $this->getCurrentCategory();
            if ($title = $category->getMetaTitle()) {
                $headBlock->setTitle($title);
            }
            if ($description = $category->getMetaDescription()) {
                $headBlock->setDescription($description);
            }
            if ($keywords = $category->getMetaKeywords()) {
                $headBlock->setKeywords($keywords);
            }
            // Add canonical
			$headBlock->addLinkRel('canonical', $category->getUrl());
			
			$hasParams = false;
			
			// Base params
			$hasParams = (	   !is_null(Mage::app()->getRequest()->getParam('p')) 
				|| !is_null(Mage::app()->getRequest()->getParam('order')) 
				|| !is_null(Mage::app()->getRequest()->getParam('dir'))
				|| !is_null(Mage::app()->getRequest()->getParam('manufacturer'))
			);
			
			// Price / designer
			foreach($category->getAvailableSortByOptions() as $key => $value) {
				$hasParams = $hasParams || !is_null(Mage::app()->getRequest()->getParam($key));
			}
			
			// Position/name/price
			$layer = Mage::getModel("catalog/layer");
			$attributes = $layer->getFilterableAttributes();
			foreach ($attributes as $attribute) {
				$hasParams = $hasParams || !is_null(Mage::app()->getRequest()->getParam($attribute->getAttributeCode()));
			}
		
            // No 301 for sorting options
			if($hasParams) {
				// Do nothing
            }
            else if($category->getUrl() != Mage::helper('core/url')->getCurrentUrl()) {
				//Mage::app()->getResponse()->setRedirect($product->getUrlModel()->getUrl($product, $params), 301);
				header ('HTTP/1.1 301 Moved Permanently');
				header ('Location: ' . $category->getUrl());
				exit;
			}
            /*
            want to show rss feed in the url
            */
            if ($this->IsRssCatalogEnable() && $this->IsTopCategory()) {
                $title = $this->helper('rss')->__('%s RSS Feed',$this->getCurrentCategory()->getName());
                $headBlock->addItem('rss', $this->getRssLink(), 'title="'.$title.'"');
            }
        }

        return $this;
    }
}